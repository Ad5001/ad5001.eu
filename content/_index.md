---
title: Main page
description: Main page and index of the website.
layout: main-index

HeaderDescription: |
    ## Full stack developer.
    I create many different kinds of software you can find over my [applications' website](https://apps.ad5001.eu).

Links:
  - Title: About me
    Icon: person
    URL: /about/
  - Title: Apps Website
    Icon: apps
    URL: https://apps.ad5001.eu

Cards:
    List:
    - FLOSS
    - Privacy
    - Platforms
    FLOSS:
        Color: blue
        Orientation: topleft2bottomright
        Icon: free
        Title: Free, Libre and Open Source
        Description: |
            The software I publish follow the [FLOSS (Free, Libre and Open Source Software)](https://en.wikipedia.org/wiki/Free_and_open-source_software) philosophy and are published under licenses that follow it like:
            - the [GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.html) 
            - the [GNU Affero General Public License v3](https://www.gnu.org/licenses/agpl-3.0.html) 
            - the [Mozilla Public License v2](https://www.mozilla.org/en-US/MPL/2.0/).   
            
            while my creative work is generally published under the [Creative Commons BY-NC-SA v4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode).
        Links:
          - Title: About FLOSS licenses
            Icon: license
            NewTab: true
            URL: https://en.wikipedia.org/wiki/Free-software_license
    Privacy:
        Color: purple
        Orientation: top2bottom
        Icon: privacy
        Title: Private by design
        Description: |
            Being FLOSS, the source code of the software I publish is fully available [on my git server](https://git.ad5001.eu), making it transparent to all.    
            As such, the software I create and publish does not [phone home](https://en.wikipedia.org/wiki/Phoning_home) or send data to the internet, unless explicitly consented by the user.    
            Some of the software I created like [AccountFree](https://apps.ad5001.eu/accountfree) or [unchecker](https://apps.ad5001.eu/unchecker) were made to solve issues related to the lack of respect for people's privacy from some companies.
        Links:
          - Title: Apps website
            Icon: apps
            URL: https://apps.ad5001.eu
    Platforms:
        Color: yellow
        Orientation: topright2bottomleft
        Icon: platforms
        Title: OS agnostic
        Description: |
            By using platform abstractions, whether made by hand, or using [frameworks](https://en.wikipedia.org/wiki/Software_framework), the software I create is available on a wide variety of platforms.    
            While I, of course, cannot package for every single distribution system out there, I try to prepare at least one distribution platform for every single operating system the software I create supports.
        Links:
          - Title: Check distribution platforms
            Icon: platforms
            URL: /distribution-platforms/

NewSection:
    Title: New website
    Description: You're viewing right now the newest version of my website. The my legacy websites can still be accessed via the links below.
    Links:
      - Title: Legacy download website
        Icon: open-external
        URL: https://download.ad5001.eu/en/
      - Title: Legacy projects website
        Icon: open-external
        URL: https://projects.ad5001.eu/en/
      - Title: Legacy API website
        Icon: open-external
        URL: https://api.ad5001.eu/en/
---
