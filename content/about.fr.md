---
title: À propos
description: Aujourd'hui en freelance, je programme maintenant depuis près de 8 ans, notamment en open source.
slug: a-propos
layout: default
---
<section class="section1">
<br><br>
<div class="columns-container padding-container-3" >
<div class="column col2 vertical-center padding-container">

# À mon propos

Aujourd'hui en freelance, je programme maintenant depuis près de 8 ans, notamment en open source.    
J'ai commencé à programmer des plugins en 2015 en PHP pour [PocketMine](https://pmmp.io), un projet au sein duquel j'ai cofondé avec d'autres contributeurs le groupe [BoxOfDevs](https://github.com/BoxOfDevs).   

Entre 2017 et 2021, j'ai travaillé sur [{{< appIcon "accountfree" >}}AccountFree](https://apps.ad5001.eu/accountfree/), aujourd'hui non fini, mais qui le sera un jour.    

Depuis le début de l'année 2021, je travaille sur [{{< appIcon "logarithmplotter" >}}LogarithmPlotter](https://apps.ad5001.eu/logarithmplotter/), et ai refais [ce site web](https://apps.ad5001.eu/ad5001.eu/) depuis le début. Je maintient aussi des extensions pour le navigateur Firefox tel que [{{< appIcon "unchecker" >}}Unchecker](https://apps.ad5001.eu/unchecker/).

</div>
<div class="column col2 vertical-center">
<div class="columns-container">
{{< card 2 "tea" "topleft2bottomright" "git" "Gitea" "open-external" "Mon Profile Gitea" "https://git.ad5001.eu/Ad5001" true false true >}}
Vous pouvez trouver tous logiciels open source que je crée sur [git.ad5001.eu, une instance Gitea](https://git.ad5001.eu). Les artefacts se trouvent sur [ci.ad5001.eu, une instance Drone CI](https://ci.ad5001.eu) et sur [leur logiciel de gestion dédié](https://artifacts.ad5001.eu).
{{< /card >}}
{{< card 2 "ebon text-white" "topleft2bottomright" "github" "Github" "open-external" "Mon Profile Github" "https://github.com/Ad5001" true false true >}}
La majorité des logiciels que je crée sont également disponibles sur [Github](https://github.com). Cependant, quelques dépôts n'y sont pas disponibles et il est recommandé de contribuer sur mon instance Gitea.
{{< /card >}}
</div>
</div>
</div>
</section>

<br>

{{< sectionSkewed "#D2D2D2" "#EEEEEE" >}}
<h1 class="text-center">Mes valeurs</h1>

<div class="columns-container padding-container-3" >
{{< card 2 "purple" "topleft2bottomright" "privacy" "Protection de la vie privée" >}}
Le droit à la vie privée est un [droit de l'homme fondamental](https://www.un.org/en/about-us/universal-declaration-of-human-rights), et c'est l'une des valeurs fondamentales que je promeus et défends.   
Il est souvent mis à mal par des entreprises dont le modèle économique repose uniquement sur le fait d'en savoir plus sur vous, et je travaille activement à réduire la dépendance aux produits de ces entreprises, notamment en promouvant le [self hosting](https://www.computerhope.com/jargon/s/self-hosting.htm), ainsi qu'en créant des logiciels qui vous aident à protéger votre vie privée en ligne comme :
- [{{< appIcon "accountfree" >}}AccountFree](https://apps.ad5001.eu/accountfree/), qui contribue à vous protéger contre l'exploitation des données par les sociétés de réseaux sociaux.

- l'extension web [{{< appIcon "unchecker" >}}unchecker](https://apps.ad5001.eu/unchecker/), qui aide à lutter contre les sites web semi-conformes au RGPD.
{{< /card >}}

{{< card 2 "blue" "top2bottom" "transparent" "Transparence" >}}
La transparence est l'un des principaux facteurs qui déterminent la fiabilité d'un projet.    
L'open source est l'une des composantes les plus importantes de la transparence d'un projet informatique, car l'obligation de publier l'ensemble du code source est un bon moyen de dissuasion contre l'imposition de fonctionnalités non éthiques.   
L'open source en soi n'est pas la seule condition pour la transparence totale d'un projet, mais avec une licence appropriée et un processus de construction transparent, il aide à encourager la confiance dans l'utilisation des projets open source.    
Vous pouvez trouver l'historique des créations de mes projets sur mon [profil Gitea](https://git.ad5001.eu/Ad5001) ou mon [profil Github](https://github.com/Ad5001), ainsi que les journaux de construction sur [Drone CI](https://ci.ad5001.eu) et les constructions sur mon [instance Nexus 3](https://artifacts.accountfree.org).
{{< /card >}}

</div>
{{< /sectionSkewed >}}

<h1 class="text-center">Mes compétences</h1>

{{< sectionSkewed "#FFEA7F" "#FEFF95" >}}
<details>
{{< summary >}}
<i class="icon-code"></i> Langages de programmation
{{< /summary >}}

Les logiciels peuvent prendre une grande variété de formes, et l'un des principes fondamentaux lors de la création de quelque chose est de *choisir le bon outil*. Chaque langage de programmation a ses avantages et ses inconvénients, et la création d'une telle variété de logiciels m'a amené à découvrir et à apprendre un certain nombre de langages.

{{< previewSkills "ProgramingLanguages" >}}


{{< /sectionSkewed >}}

{{< sectionSkewed "#96DCB9" "#9EE7C0" >}}
<details>
{{< summary >}}
<i class="icon-frameworks"></i> Frameworks
{{< /summary >}}

Les frameworks sont des composants très courants dans la création de logiciels. Cependant, dans l'idée de rester simple, je m'efforce d'en utiliser le moins possible, car ils ont tendance à ajouter des surcharges et des pertes de performance.

Voici donc une liste des frameworks que j'ai utilisés dans le passé :

## Web Frameworks Web
{{< previewSkills "FrameworksWeb" >}}

## Frameworks d'Applications
{{< previewSkills "FrameworksDev" >}}


Comme mon expérience avec les frameworks grandissait, j'ai commencé à en créer moi-même que j'utilise dans certains de mes logiciels.

## Frameworks que j'ai créé.
{{< previewSkills "FrameworksCreated" >}}


{{< /sectionSkewed >}}

{{< sectionSkewed "#6DB4FF" "#91C6FF" >}}
<details>
{{< summary >}}
<i class="icon-linux"></i> Administration Système
{{< /summary >}}

J'utilise Linux depuis 5 ans, tant sur les ordinateurs de bureau que sur les serveurs, car il m'a aidé et continue de m'aider à devenir beaucoup plus compétent.   
Bien que j'aie essayé de nombreuses distributions, voici celles avec lesquelles j'ai le plus d'expérience :   

## Distributions Linux
{{< previewSkills "LinuxDistribs" >}}

Cette expérience m'a également permis de découvrir les technologies de conteneurs :


## Technologies des conteneurs
{{< previewSkills "ContainerTechnologies" >}}


Avec ces technologies de conteneurs, j'ai commencé à utiliser beaucoup plus les technologies de self hosting, notamment :

## Self hosting
{{< previewSkills "SelfHosted" >}}

{{< /sectionSkewed >}}

<section class="section1">

# Me contacter

Le principal moyen de me contacter est <a alt="Mail">par <i class="icon-mail"></i> courriel</a>.    
Cependant, vous pouvez également me contacter via [<i class="icon-git"></i> Gitea](https://git.ad5001.eu) ou [<i class="icon-mastodon"></i> Mastodon](https://fosstodon.org/@Ad5001) pour les besoins liés au développement.    
Je ne suis pas vraiment actif sur [<i class="icon-github"></i> Github](https://github.com), mais vous pouvez toujours me contacter là-bas. Toutefois, sachez qu'il se peut que je mette du temps à vous répondre.

<br>

<p class="text-center"><i>Tous les noms de produits, logos et marques sont la propriété de leurs détenteurs respectifs. Tous les noms de sociétés, de produits et de services utilisés sur ce site Web ne le sont qu'à des fins d'identification. L'utilisation de ces noms, logos et marques n'implique pas leur approbation.</i></p>

<br>
</section>
