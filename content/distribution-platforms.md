---
title: Distribution platforms
description: Lists all distribution platforms I support and provides information about badges.
layout: default
---
<section class="section1">
<br><br><br>

# Distribution platforms

The software I distribute is available on many platforms. However, distributing it solely on my website does not help their visibility, nor their ease of installation. As such, like many other, I publish my software on certain distribution platforms.

## List of platforms on which I published at least one software:
- <img src="/icons/distribution/firefox.svg" alt="Firefox icon" class="icon-larger-text"/>**Firefox Addons** located on [addons.mozilla.org](https://addons.mozilla.org) under [my profile](https://addons.mozilla.org/en-US/firefox/user/12994006/).
- <img src="/icons/distribution/snapcraft.svg" alt="Snapcraft icon" class="icon-larger-text"/>**Snapcraft** located on [snapcraft.io](https://snapcraft.io).
- <img src="/icons/distribution/flathub.svg" alt="flathub icon" class="icon-larger-text"/>**Flathub** located on [flathub.org](https://flathub.org).

<br><br>
</section>
{{< sectionSkewed "#D2D2D2" "#EEEEEE" >}}

# Badges
The majority of these distribution platforms provide a **badge** to indicate that their is available on the publisher's store.     
Unfortunately, the majority of these badges are only available in English, or are out of date with their current design.   
For that reason, I've created (or recreated in some cases) the badges for each store I produce to fit both their design guidelines and Omegamma's design.     

To include these badges on your own website, you should include the `badge` stylesheet using this HTML Link:    
`<link rel="stylesheet" href="https://ad5001.eu/css/badge.css">`    
and then include whatever badge you need.

**Note**: The logos on these badges are copyrighted by their owners, and appropriate copyright is given with them.

{{< /sectionSkewed >}}
{{< sectionSkewed "#FFEA7F" "#FEFF95" >}}
<details>
{{< summary >}}
<img src="/icons/distribution/firefox.svg" alt="Firefox icon" class="icon-larger-text shadowed"/>Firefox Addons
{{< /summary >}}
Also known as AMO and hosted at [addons.mozilla.org](https://addons.mozilla.org), this store is exclusive to Firefox extensions.

{{< badgeTable "amo" "storeFirefoxAddons" >}}

**Disclaimer**: The Firefox logo is a trademark of the Mozilla Foundation in the U.S. and other countries.   
This badge is the translated and updated form of [the old badge](https://blog.mozilla.org/addons/2015/11/10/promote-your-add-ons-with-the-get-the-add-on-button/) with the new AMO colors and branding, and uses the font **Sharp Sans**.
</details>
{{< /sectionSkewed >}}
{{< sectionSkewed "#96DCB9" "#9EE7C0" >}}
<details>
{{< summary >}}
<img src="/icons/distribution/snapcraft.svg" alt="Snapcraft icon" class="icon-larger-text shadowed"/>Snap Store
{{< /summary >}}
Also known as Snapcraft and hosted at [snapcraft.io](https://snapcraft.io), this store is used to publish snaps, a Linux distribution format created by [Canonical](https://canonical.com/) (the company behind Ubuntu) and supported on a wide varierty of Linux distributions.

{{< badgeTable "snapcraft" "storeSnapcraft" >}}

**Disclaimer**: The Snapcraft logo is licensed under [CC BY-ND 2.0 UK](https://creativecommons.org/licenses/by-nd/2.0/uk/legalcode), a registered trademark of Canonical Limited, 2018.    
These icons are adapted versions of [the official snapcraft badges](https://github.com/snapcore/snap-store-badges), and uses the font **Ubuntu**.   

{{< /sectionSkewed >}}
{{< sectionSkewed "#6DB4FF" "#91C6FF" >}}
<details>
{{< summary >}}
<img src="/icons/distribution/flathub.svg" alt="Flathub icon" class="icon-larger-text shadowed"/>Flathub
{{< /summary >}}
Main repository of the decentralized [Flatpak](https://flatpak.org/) system and hosted at [flathub.org](https://flathub.org), this store is used to publish snaps, a widely supported Linux distribution format.

{{< badgeTable "flathub" "storeFlathub" >}}

**Legal notice**: These icons are modified and translated versions of [the official flathub badges](https://flathub.org/badges), the official ones being under [CC0](http://creativecommons.org/publicdomain/zero/1.0/), and uses the font **Source Sans Pro**.

{{< /sectionSkewed >}}
<section class="section1">

## Want to add a badge or translate them in your language?

### Translating a badge:
1. Check if the official badge sources don't have a translation already, if so, it's text will be used as reference.
2. Download the existing English `-text.svg` badge.
3. Translate the text. You can use software like [Inkscape](https://inkscape.org) to edit the SVG directly.
4. [Fork Omegamma](https://git.ad5001.eu/Ad5001/Omegamma).
5. Create, if it does not exist already, a directory with your language code.
6. If the directory did not exist before, make sure to copy the `export.sh` script from one of the other directories.
7. Put the badge under `<store name>-text.svg`.
8. (Optional, only if all store badges are translated) Register the language in the `badgeTable.html` shortcode.
9. Create a pull request for merging and adding your badge to Omegamma.

### Creating a new badge:
1. Check if an official badge source for that store doesn't exist and their potential associated trademarks.
2. If so, download the SVG version of the badge. Otherwise create a new SVG. You can use software like [Inkscape](https://inkscape.org) to edit the SVG directly.
3. The SVG item must have a solid background, with no border, or rounded corners. If the official source has some, remove them.
4. [Fork Omegamma](https://git.ad5001.eu/Ad5001/Omegamma).
5. For every language your new badge supports, put the icon under `<language>/<store name>-text.svg`.
6. If your icon is subject to trademark, make sure you have the right to use it for that context, and add the trademark text to the repository's `README.md`.
7. (Optional, only if store badge is available in all languages) Register the icon in the `distribution-platforms.*.md` content file on [ad5001.eu's source code](https://git.ad5001.eu/Ad5001/ad5001.eu).
8. Create a pull request for merging and adding your badge to Omegamma.

You can find download existing badges for reference in
<a role="button" class="primary" rel="noreferrer" target="_blank" href="https://git.ad5001.eu/Ad5001/Omegamma/src/branch/master/assets/img/badges">
<i class="icon-git"></i> Omegamma's badge directory
</a>.
</section>
