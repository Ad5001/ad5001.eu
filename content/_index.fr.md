---
title: Page principale
description: Page principale et index du site web.
layout: main-index

HeaderDescription: |
    ## Programmeur full-stack.
    Je crée de nombreux types de logiciels que vous pouvez trouver sur mon [site d'applications](https://apps.ad5001.eu).

Links:
  - Title: À propos
    Icon: person
    URL: /fr/a-propos/
  - Title: Site d'apps
    Icon: apps
    URL: https://apps.ad5001.eu/fr/

Cards:
    List:
    - FLOSS
    - Privacy
    - Platforms
    FLOSS:
        Color: blue
        Orientation: topleft2bottomright
        Icon: free
        Title: Libre, gratuit et open source
        Description: |
            Les logiciels que je publie suivent la philosophie [FLOSS (Free, Libre and Open Source Software)](https://fr.wikipedia.org/wiki/Logiciel_libre) et sont publiés sous des licences qui la suivent comme:
            - la [Licence publique générale GNU v3](https://www.gnu.org/licenses/gpl-3.0.fr.html) 
            - la [Licence publique générale GNU Affero v3](https://www.gnu.org/licenses/agpl-3.0.fr.html) 
            - la [Licence publique Mozilla v2](https://www.mozilla.org/en-US/MPL/2.0/).   
            
            tandis que mon travail créatif est généralement publié sous la licence [Creative Commons BY-NC-SA v4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.fr).
        Links:
          - Title: À propos des licences FLOSS
            Icon: license
            NewTab: true
            URL: https://fr.wikipedia.org/wiki/Logiciel_libre#Licences_libres
    Privacy:
        Color: purple
        Orientation: top2bottom
        Icon: privacy
        Title: Privé par nature
        Description: |
            En tant que FLOSS, le code source des logiciels que je publie est entièrement disponible [sur mon serveur git](https://git.ad5001.eu), ce qui le rend transparent pour tous.    
            De ce fait, les logiciels que je crée et publie ne [téléphone pas à la maison](https://en.wikipedia.org/wiki/Phoning_home) ni n'envoie de données sur l'internet, à moins que l'utilisateur n'y consente explicitement.    
            Certains des logiciels que j'ai créés, comme [AccountFree](https://apps.ad5001.eu/fr/accountfree) ou [unchecker](https://apps.ad5001.eu/fr/unchecker), ont été conçus pour résoudre des problèmes liés au manque de respect de la vie privée de certaines entreprises.
        Links:
          - Title: Site d'apps
            Icon: apps
            URL: https://apps.ad5001.eu/fr/
    Platforms:
        Color: yellow
        Orientation: topright2bottomleft
        Icon: platforms
        Title: Disponible partout
        Description: |
            En utilisant des abstractions de plateforme, qu'elles soient faites à la main ou à l'aide de [frameworks](https://en.wikipedia.org/wiki/Software_framework), les logiciels que je crée sont disponibles sur une grande variété de plateformes.    
            Bien que je ne puisse évidemment pas empaqueter pour chaque système de distribution existant, j'essaie d'utiliser au moins une plateforme de distribution pour chaque système d'exploitation que le logiciel supporte.
        Links:
          - Title: Liste les plateformes de distribution
            Icon: platforms
            URL: /fr/plateformes-de-distribution/

NewSection:
    Title: Nouveau site web.
    Description: Vous êtes en train de consulter la toute dernière version de mon site web. Les anciens sites web sont toujours accessibles via les liens ci-dessous.
    Links:
      - Title: Ancien site de téléchargements
        Icon: open-external
        NewTab: true
        URL: https://download.ad5001.eu/fr/
      - Title: Ancien site des projects 
        Icon: open-external
        NewTab: true
        URL: https://projects.ad5001.eu/fr/
      - Title: Ancien site d'API
        Icon: open-external
        NewTab: true
        URL: https://api.ad5001.eu/fr/
---

