---
title: Plateformes de distribution
description: Répertorie toutes les plateformes de distribution que je supporte et fournit des informations sur les badges.
slug: plateformes-de-distribution
layout: default
---
<section class="section1">
<br><br><br>

# Plateformes de distribution

Les logiciels que je distribue sont disponibles sur de nombreuses plateformes. Cependant, les distribuer uniquement sur mon site web n'aide pas leur visibilité, ni leur facilité d'installation. Ainsi, comme beaucoup d'autres, je publie mes logiciels sur certaines plateformes de distribution.

## Liste des plateformes sur lesquelles j'ai publié au moins un logiciel:
- <img src="/icons/distribution/firefox.svg" alt="Firefox icon" class="icon-larger-text"/>**Firefox Addons** situé sur [addons.mozilla.org](https://addons.mozilla.org) sous [mon profile](https://addons.mozilla.org/en-US/firefox/user/12994006/).
- <img src="/icons/distribution/snapcraft.svg" alt="Snapcraft icon" class="icon-larger-text"/>**Snapcraft** situé sur [snapcraft.io](https://snapcraft.io).
- <img src="/icons/distribution/flathub.svg" alt="flathub icon" class="icon-larger-text"/>**Flathub** situé sur [flathub.org](https://flathub.org).

<br><br>
</section>
{{< sectionSkewed "#D2D2D2" "#EEEEEE" >}}

# Badges
La majorité de ces plateformes de distribution fournissent un **badge** pour indiquer que leur est disponible sur la boutique de l'éditeur.     
Malheureusement, la majorité de ces badges ne sont disponibles qu'en anglais, ou ne sont pas à jour avec le design actuel de leur boutique.   
Pour cette raison, j'ai créé (ou recréé dans certains cas) les badges pour chaque plateforme que je produis afin qu'ils correspondent à la fois à leur charte graphique et à celle d'omegamma.     

Pour inclure ces badges sur votre propre site web, vous devez inclure la feuille de style `badge` en utilisant ce lien HTML :    
`<link rel="stylesheet" href="https://ad5001.eu/css/badge.css">`    
puis inclure le badge dont vous avez besoin.
    
**Note** : Les logos sur ces badges sont protégés par les droits d'auteur de leurs propriétaires, et un copyright approprié est donné avec eux.

{{< /sectionSkewed >}}
{{< sectionSkewed "#FFEA7F" "#FEFF95" >}}
<details>
{{< summary >}}
<img src="/icons/distribution/firefox.svg" alt="Firefox icon" class="icon-larger-text shadowed"/>Firefox Addons
{{< /summary >}}
Également connue sous le nom d'AMO et hébergée à l'adresse [addons.mozilla.org](https://addons.mozilla.org), cette plateforme est exclusive aux extensions Firefox.

{{< badgeTable "amo" "storeFirefoxAddons" >}}

**Disclaimer** : Le logo Firefox est une marque déposée de la Fondation Mozilla aux États-Unis et dans d'autres pays.   
Ce badge est la forme traduite et mise à jour de [l'ancien badge](https://blog.mozilla.org/addons/2015/11/10/promote-your-add-ons-with-the-get-the-add-on-button/) avec les nouvelles couleurs et le nouveau thème d'AMO, et utilisent la police **Sharp Sans**.
</details>
{{< /sectionSkewed >}}
{{< sectionSkewed "#96DCB9" "#9EE7C0" >}}
<details>
{{< summary >}}
<img src="/icons/distribution/snapcraft.svg" alt="Snapcraft icon" class="icon-larger-text shadowed"/>Snap Store
{{< /summary >}}
Également connu sous le nom de Snapcraft et hébergé sur [snapcraft.io](https://snapcraft.io), ce magasin est utilisé pour publier des snaps, un format de distribution Linux créé par [Canonical](https://canonical.com/) (la société à l'origine d'Ubuntu) et pris en charge par de nombreuses distributions Linux.

{{< badgeTable "snapcraft" "storeSnapcraft" >}}

**Disclaimer** : Le logo Snapcraft est sous licence [CC BY-ND 2.0 UK](https://creativecommons.org/licenses/by-nd/2.0/uk/legalcode), une marque déposée de Canonical Limited, 2018.    
Ces icônes sont des versions adaptées des [badges officiels de snapcraft](https://github.com/snapcore/snap-store-badges), et utilisent la police **Ubuntu**.

{{< /sectionSkewed >}}
{{< sectionSkewed "#6DB4FF" "#91C6FF" >}}
<details>
{{< summary >}}
<img src="/icons/distribution/flathub.svg" alt="Flathub icon" class="icon-larger-text shadowed"/>Flathub
{{< /summary >}}
Dépôt principal du système décentralisé [Flatpak](https://flatpak.org/) et hébergé sur [flathub.org](https://flathub.org), ce magasin est utilisé pour publier des flatpaks, un format de distribution Linux largement supporté.

{{< badgeTable "flathub" "storeFlathub" >}}

**Avis légal** : Ces icônes sont des versions modifiées et traduites des [badges officiels de flathub](https://flathub.org/badges), les badges officiels étant sous [CC0](http://creativecommons.org/publicdomain/zero/1.0/), et utilisent la police **Source Sans Pro**.

{{< /sectionSkewed >}}
<section class="section1">

## Vous voulez ajouter un badge ou les traduire dans votre langue ?

### Traduire un badge :
1. Vérifiez si les sources officielles du badge n'ont pas déjà une traduction, si c'est le cas, le texte sera utilisé comme référence.
2. Téléchargez le badge en Anglais existant `-text.svg`.
3. Traduisez le texte. Vous pouvez utiliser un logiciel comme [Inkscape](https://inkscape.org) pour éditer directement le SVG.
4. [Forkez Omegamma](https://git.ad5001.eu/Ad5001/Omegamma).
5. Créez, s'il n'existe pas déjà, un répertoire avec votre code de langue.
6. Si le répertoire n'existait pas auparavant, assurez-vous de copier le script `export.sh` depuis l'un des autres répertoires.
7. Mettez le badge sous `<store name>-text.svg`.
8. (Facultatif, seulement si tous les badges des magasins sont traduits) Enregistrez la langue dans le shortcode `badgeTable.html`.
9. Créez une pull-request  et ajouter votre badge à Omegamma.

### Créer un nouveau badge :
1. Vérifiez s'il n'existe pas de source officielle de badge pour ce magasin et les éventuelles marques associées.
2. Si c'est le cas, téléchargez la version SVG du badge. Sinon, créez un nouveau SVG. Vous pouvez utiliser un logiciel comme [Inkscape](https://inkscape.org) pour modifier directement le SVG.
3. Le SVG doit avoir un fond uni, sans bordure, ni coins arrondis. Si la source officielle en possède, supprimez-les.
4. [Forkez Omegamma](https://git.ad5001.eu/Ad5001/Omegamma).
5. Pour chaque langue que votre nouveau badge supporte, mettez l'icône sous `<langue>/<nom du magasin>-text.svg`.
6. Si votre icône est soumise à une marque déposée, assurez-vous que vous avez le droit de l'utiliser dans ce contexte, et ajoutez le texte de la marque déposée au `README.md` du dépôt.s
7. (Facultatif, uniquement si le badge de la boutique est disponible dans toutes les langues) Enregistrez l'icône dans le fichier de contenu `distribution-platforms.*.md` sur [le code source de ad5001.eu](https://git.ad5001.eu/Ad5001/ad5001.eu).
8. Créez une pull-request et ajouter votre badge à Omegamma.

Vous pouvez télécharger les badges existants pour référence dans le
<a role="button" class="primary" rel="noreferrer" target="_blank" href="https://git.ad5001.eu/Ad5001/Omegamma/src/branch/master/assets/img/badges">
<i class="icon-git"></i> répertoire des badges d'Omegamma
</a>.
</section>
