---
title: About me
description: Currently a freelancer, I have been programming for almost 8 years, mainly in open source.
layout: default
---
<section class="section1">
<br><br>
<div class="columns-container padding-container-3" >
<div class="column col2 vertical-center padding-container">

# About me

Currently a freelancer, I have been programming for almost 8 years, mainly in open source.    
I've started programming plugins in 2015 in PHP for [PocketMine](https://pmmp.io), a project within which, alongside other contributors, I helped co-founding the [BoxOfDevs](https://github.com/BoxOfDevs) group.   

Between 2017 and 2021, I worked on [{{< appIcon "accountfree" >}}AccountFree](https://apps.ad5001.eu/accountfree/), unfinished as of today, though I may finish it some day.    

Since the beginning of 2021, I've been working on [{{< appIcon "logarithmplotter" >}}LogarithmPlotter](https://apps.ad5001.eu/logarithmplotter/), and remaked [this website](https://apps.ad5001.eu/ad5001.eu/) from scratch. I'm also maintaining several extension for Firefox, including [{{< appIcon "unchecker" >}}Unchecker](https://apps.ad5001.eu/unchecker/).

</div>
<div class="column col2 vertical-center">
<div class="columns-container" >
{{< card 2 "tea" "topleft2bottomright" "git" "Gitea" "open-external" "My Gitea Profile" "https://git.ad5001.eu/Ad5001" true false true >}}
You can find the various open source softwares I've created on [git.ad5001.eu, a Gitea instance](https://git.ad5001.eu). Their artifacts can be found on [its dedicaded webpage](https://artifacts.ad5001.eu).
{{< /card >}}
{{< card 2 "ebon text-white" "topleft2bottomright" "github" "Github" "open-external" "My Github Profile" "https://github.com/Ad5001" true false true >}}
The majority of the software I've created is also available on [Github](https://github.com). However, there a few repositories that might not be available there, and contributions are recommended to be done on Gitea.
{{< /card >}}
</div>
</div>
</div>
</section>

<br>

{{< sectionSkewed "#D2D2D2" "#EEEEEE" >}}
<h1 class="text-center">My values</h1>

<div class="columns-container padding-container-3" >
{{< card 2 "purple" "topleft2bottomright" "privacy" "Privacy" >}}
Right to privacy is a fundamental [human right](https://www.un.org/en/about-us/universal-declaration-of-human-rights), and it's one of the core values I promote and defend.   
It is often undermined by companies whose business model rely solely on knowing more about you, and I actively work to reduce the dependence on these companies' products, including by promoting [self hosting](https://www.computerhope.com/jargon/s/self-hosting.htm), as well as creating software that helps you protecting your privacy online like:
- [{{< appIcon "accountfree" >}}AccountFree](https://apps.ad5001.eu/accountfree/), which helps protecting you from being data mined by social network companies

- the web extension [{{< appIcon "unchecker" >}}unchecker](https://apps.ad5001.eu/unchecker/) which helps against semi-GDPR compliant websites.
{{< /card >}}

{{< card 2 "blue" "top2bottom" "transparent" "Transparency" >}}
Transparency is one of the greatest factors when it comes to how trustworthy a project is.    
Open source is one of the most important components when it comes to how transparent an IT project is, as it the requirement to publish all source code is a good deterrent against imposing unethical functionality.   
Open source in itself isn't the sole requirement for a project's full transparency, but alongside proper licensing and a transparent building process, it helps fostering proper trust for using open source projects.    
You can find the history of my projects creations either on my [Gitea profile](https://git.ad5001.eu/Ad5001) or my [Github profile](https://github.com/Ad5001), as well as build logs on [Drone CI](https://ci.ad5001.eu) and the builds on my [Nexus 3 instance](https://artifacts.accountfree.org).
{{< /card >}}

</div>
{{< /sectionSkewed >}}

<h1 class="text-center">My skills</h1>

{{< sectionSkewed "#FFEA7F" "#FEFF95" >}}
<details>
{{< summary >}}
<i class="icon-code"></i> Programming Languages
{{< /summary >}}

Software can take a wide variety of forms, and one of the core principles when creating something is *choosing the right tool for the job*. Each programming languages has it's advantages and drawbacks, and creating such a wide variety of software has led me to discover and learn quite a few languages.

{{< previewSkills "ProgramingLanguages" >}}


{{< /sectionSkewed >}}

{{< sectionSkewed "#96DCB9" "#9EE7C0" >}}
<details>
{{< summary >}}
<i class="icon-frameworks"></i> Frameworks
{{< /summary >}}

Frameworks are very common components in software creation. However, with the idea to keep it simple, I strive to use as few of them as possible, because they tend to add performance overhead and loss.

Hence, here is a list of frameworks I've used in the past:

## Web frameworks
{{< previewSkills "FrameworksWeb" >}}

## Application frameworks
{{< previewSkills "FrameworksDev" >}}


As my experience with frameworks grew, I began creating some myself that I use in some of my software.

## Frameworks I created
{{< previewSkills "FrameworksCreated" >}}


{{< /sectionSkewed >}}

{{< sectionSkewed "#6DB4FF" "#91C6FF" >}}
<details>
{{< summary >}}
<i class="icon-linux"></i> System Administration
{{< /summary >}}

I've been using Linux for the past 5 years both on desktop and servers, as it helped and continues to help me become far more proficient with it.   
While I've tried many distributions, here are the ones I have the most experience with:   

## Linux distributions
{{< previewSkills "LinuxDistribs" >}}

This experience also made me discover container technologies:


## Container technologies
{{< previewSkills "ContainerTechnologies" >}}


With these container technologies, I began using self-hosting technologies a lot more, including:

## Self hosting
{{< previewSkills "SelfHosted" >}}

{{< /sectionSkewed >}}

<section class="section1">

# Contact me

The primary mean to contact me is <a alt="Mail">by <i class="icon-mail"></i> email</a>.    
However, you can also contact me through [<i class="icon-git"></i> Gitea](https://git.ad5001.eu) or [<i class="icon-mastodon"></i> Mastodon](https://fosstodon.org/@Ad5001) for development related purposes.    
I'm not really active on [<i class="icon-github"></i> Github](https://github.com), but you can still contact me there. However, do except it may take time for me to respond.

<br>

<p class="text-center"><i>All product names, logos, and brands are property of their respective owners. All company, product and service names used in this website are for identification purposes only. Use of these names, logos, and brands does not imply endorsement.</i></p>

<br>
</section>
