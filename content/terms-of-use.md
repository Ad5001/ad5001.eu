---
title: Terms of use
description: Terms of use for ad5001.eu and related websitest
layout: default
---
<section class="section1">
<br><br><br>

# Terms of use & privacy policy.

## Introduction

These terms of use covers your usage of ad5001.eu, apps.ad5001.eu, download.ad5001.eu, projects.ad5001.eu, api.ad5001.eu, git.ad5001.eu, accountfree.org, services.accountfree.org, and docs.accountfree.org.

1. The service is provided "as-is". Use of the service is at your own risk.
2. You must not modify another site in order to falsely signify that it is associated with this service ad5001.eu.
3. Accounts can only be created and used by humans on some of these websites. Accounts created by robots or other automated methods may be deleted without notice.
4. You are responsible for the security of your account and password. ad5001.eu can not and will not be responsible for any losses or damage resulting from your failure to comply with this safety obligation.
5. You are responsible for all content posted and activity that occurs under your account.
6. You may not use the Service for any illegal or unauthorized purpose. You must not break the laws of your country.
7. You may not sell, trade, resell, or exploit in an unauthorized business purpose an account of the service used.

Violation of any of these agreements will result in termination of your account. You understand and accept that the ad5001.eu association cannot be held responsible for the content published on this service.

1. You understand that putting the service and your content online involves transmission (in plain text or encrypted, depending on the service) over various networks.
2. You must not transmit worms, viruses or any other malicious code.
3. ad5001.eu does not guarantee that
    - the service will meet your specific needs,
    - the service will be uninterrupted or bug-free,
    - that errors in the service will be corrected.
4. You understand and agree that ad5001.eu cannot be held responsible for any direct, indirect or incidental damages, including damages for loss of profits, customers, access, data or other intangible losses (even if ad5001.eu is informed of the possibility of such damages) and that would result from:
    - use or inability to use the service;
    - unauthorized or altered access to data transmission;
    - statements or actions of a third party about the service;
    - cancelling your account;
    - any other service related question.
5. The failure of ad5001.eu to exercise or enforce any right or provision of the Terms of Use does not constitute a waiver to that right or provision. The Terms of Use constitute the entire agreement between you and ad5001.eu and govern your use of the service, replacing all previous agreements between you and ad5001.eu (including previous versions of the Terms of Use).
6. Questions about the conditions of service should be sent by mail.

## Modifications of the service

1. ad5001.eu reserves the right, at any time, to modify or interrupt, temporarily or permanently, with or without notice.
2. ad5001.eu will not be liable to you or any third party for any modification, suspension or interruption of service.

## Content copyright

1. You cannot send, upload, blog, distribute, disseminate any illegal, defamatory, harassing, abusive, fraudulent content, counterfeit, obscene or otherwise objectionable.
2. We do not claim any rights on your data: text, images, sound, video, or any other element, that you download or transmit from your account.
3. We will not use your content for any purpose other than to provide the service to you.
4. You must not download or make available any content that infringes someone else’s rights.
5. We reserve the right to remove any content we deem necessary. not relevant to the use of the service, in our sole judgment.
6. We may, if necessary, delete or prevent the distribution of any content on the service that does not comply with these conditions.

## Editing and data sharing

1. The files you create with the service can be - if you wish - read, copied, used and redistributed by people you know or not.
2. By making your data public, you acknowledge and agree that any person using this website may consult them without restrictions.
3. But the service can also offer you the possibility to authorize restricted access and collaborative work on its documents to one or more other users.
4. ad5001.eu cannot be held responsible for any problem resulting from the sharing or publishing data between users.

## Closure

1. ad5001.eu, in its sole discretion, has the right to suspend or terminate your account and refuse any current or future use of the service. Termination of the service will result in deactivation of access to your account, and the restitution of all the contents.
2. ad5001.eu reserves the right to refuse service to anyone for any reason at any time.

3. ad5001.eu also reserves the right to terminate your account if you do not log into your account for a period longer than 6 months.

## Personal information

In accordance with article 34 of the law “Informatique et Libertés”, ad5001.eu guarantees the user a right to opposition, access and of correction on the personal data concerning him. The user has the possibility to exercise this right by using the contact form available.

1. To use certain services ad5001.eu, you must create an account. ad5001.eu requests some personal information : a valid email address and a password that is used to protect your account against unauthorized access. The “Last Name” and “First Name” fields may be required for the correct operation of the software, but it is not necessary that they reveal your true identity.
2. Just like other online services, ad5001.eu automatically saves certain information about your use of the service such as account activity (example: storage space occupied, number of entries, measures taken), the data displayed or clicked (example: links, user interface elements), and other information to identify you (example: browser type, IP address, date and time of access, reference URL).
3. We use this information internally for security purposes of ad5001.eu's services and maintain a consistent and reliable user experience.
4. This data is neither sold nor passed on to third parties.

## Personal information collected

In France, personal data is protected by Act No. 78-17 of 6 January 1978, Act No. 2004-801 of 6 August 2004, article L. 226-13 of the Penal Code and the European Directive of 24  October 1995.

In any event some of ad5001.eu network websites does collect personal information about the user (name, e-mail address) that is needed by the services offered by the sites of the ad5001.eu network. The user provides this information in full knowledge of the facts, in particular when he/she inputs them him/herself. It is specified at this time to the user of the ad5001.eu network if the information to provide is obligatory or not.

## Rectification of personal information collected

In accordance with the provisions of Article 34 of Act No 48-87 of January 6, 1978, the user has a right of modification of the personal data collected concerning him. To do this, user sends to ad5001.eu:

1. Use the dedicated UI for changing your personal information
2. You can send us an e-mail.

The amendment will be made within a reasonable period of time from the date of receiving the user’s request.
Limitation of liability

This site contains information made available by external communities or companies or hypertext links to other sites that were not developed by ad5001.eu. Content made available on the site is provided for information purposes only. The existence of a link of this site to another site does not constitute a validation of that site, or of its content. It is up to the Internet user to use these information with discernment and critical thinking. The responsibility of ad5001.eu could not be committed because of the information, opinions and recommendations made by third parties.

ad5001.eu cannot be held liable for direct or consequential damages. indirect caused to the user’s material, when accessing the site, and resulting either from the use of equipment that does not meet the technical specifications required, either from the appearance of a bug or of incompatibility.

ad5001.eu cannot also be held responsible for damages. indirect (such as a loss of market or a loss of a chance) resulting from the use of the site.

Interactive spaces (possibility to ask questions in the contact area) are available to users on the site ad5001.eu. ad5001.eu reserves the right to delete, without prior formal notice, any content deposited in this space which would contravene the applicable legislation in France, in in particular data protection provisions. If necessary, ad5001.eu also reserves the possibility of putting in cause the user’s civil and/or criminal liability, in particular in the event of a racist, insulting, defamatory, or pornographic message, whatever the medium used (text, photography…).

## Contractual limitations on technical data

ad5001.eu cannot be held responsible for material damages. linked to the use of the site. In addition, the user of the site undertakes to access the site using recent material, not containing viruses and with an up-to-date browser.
Intellectual property

The contents are published under the responsibility of the users.

In France, personal data is protected by Act No. 78-17 of 6 January 1978, Act No. 2004-801 of 6 August 2004, article L. 226-13 of the Penal Code and the European Directive of 24  October 1995.

These terms are modified from [Framasoft's](https://framasoft.org/en/cgu).
</section>
<section class="section1">

# Cookie policy

These policy covers your usage of git.ad5001.eu.

## Our usage of cookies

git.ad5001.eu uses cookies, web beacons and other similar technologies to:
- Help you to access and use the Services; and
- Set your preferences;

## Opting-out

You have the option to opt-out and disable most cookies that we use in our Services. However, the disabling of cookies may cause certain features of the Services not to function properly. Any cookies that are considered strictly necessary, which are those that are essential for the Service to operate, cannot be disabled.

To opt out you may:

1. Instruct your browser, by changing its options, to stop accepting cookies or to prompt you before accepting a cookie from websites you visit; or
2. Withdraw or change your consent using the UI at git.ad5001.eu.

Most Web browsers also allow you to delete cookies already set. However, if you choose to delete cookies, the settings and preferences controlled by those cookies, may be deleted and may need to be recreated.

</section>
