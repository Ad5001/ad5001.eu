## License

- content/\* 
- data/\*
- i18n/\*
- static/icons/skills/nowox.svg
- static/icons/skills/accountfree.svg
- static/icons/skills/logarithmplotter.svg
- static/icons/skills/omegamma.svg
- static/icons/skills/bashoop.svg
    Under the [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode) with a possible exceptions for app content given in written form for [authorized distributions platforms](https://ad5001.eu/distribution-platforms).

- static/icons/skills/flatpak.svg:
    Under Creative Commons Attribution 3.0 [on flatpak's website](https://flatpak.org/press/).
    
- rest of static/icons/skills/\* :
    All product names, logos, and brandsare property of their respective owners. All company, product and service names used in this website are for identification purposes only. Use of these names, logos, and brands does not imply endorsement.    
    Most of these icons are from [devicons](https://devicon.dev).
