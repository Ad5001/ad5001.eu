# ad5001.eu
Hugo source for [ad5001.eu](https://ad5001.eu)

## Build
You need [Hugo](https://gohugo.io) to build this website.   
Clone the [Omegamma](https://git.ad5001.eu/Ad5001/Omegamma) in the `themes` directory.   
You can test it by placing yourself in the root directory and running `hugo server -w`.   
You can build the website by running `hugo`.   

## License

- content/\* 
- static/icons/skills/nowox.svg
- static/icons/skills/accountfree.svg
- static/icons/skills/logarithmplotter.svg
- static/icons/skills/omegamma.svg
    Under the [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode) with a possible exceptions for app content given in written form for [authorized distributions platforms](https://ad5001.eu/distribution-platforms).

- rest of static/icons/skills/\* :
    All product names, logos, and brandsare property of their respective owners. All company, product and service names used in this website are for identification purposes only. Use of these names, logos, and brands does not imply endorsement.    
    Most of these icons are from [devicons](https://devicon.dev).
